#   Proyecto creado por TecnoProfe
#    youtube: https://www.youtube.com/tecnoprofe
from random import random
import pygame
import os
import random


ANCHO = 1024
ALTO = 700
DIMENSIONES=(ANCHO,ALTO)
NEGRO = (0, 0, 0)
pygame.init()
ventana = pygame.display.set_mode(DIMENSIONES)
pygame.display.set_caption("Aviones")
clock = pygame.time.Clock()

#=============Cargar sonidos
# se cargar un sonido golpe
bala = pygame.mixer.Sound(os.path.dirname(__file__)+"\\sonido\\golpe07.mp3")


# ========================================
# CLASE NAVE
class Nave(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\nave2.png")
        self.image.set_colorkey(NEGRO)
        self.rect = self.image.get_rect()
        self.rect.centerx = ANCHO // 2
        self.rect.bottom = ALTO
        self.velocidad_x = 0
        self.velocidad_y = 0

    def update(self):
        self.velocidad_x = 0
        self.velocidad_y = 0
        keystate=pygame.key.get_pressed()
        
        self.image = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\nave1.png")

        # Movimiento a la izquierda
        if keystate[pygame.K_LEFT]:
            self.image = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\nave1_izq.png")
            self.velocidad_x = -7
        # Movimiento a la derecha
        if keystate[pygame.K_RIGHT]:
            self.image = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\nave1_der.png")
            self.velocidad_x = 7  
        # Movimiento arriba
        if keystate[pygame.K_UP]:
            self.velocidad_y = -7  
        # Movimiento abajo
        if keystate[pygame.K_DOWN]:
            self.velocidad_y = 7



        self.rect.x += self.velocidad_x        
        self.rect.y += self.velocidad_y
        
        # Control de salida de los bordes izquierdo y derecho.
        if self.rect.right > ANCHO:
            self.rect.right = ANCHO
        if self.rect.left < 0:
            self.rect.left = 0

        # Control de salida de los bordes arriba y abajo
        if self.rect.bottom > ALTO:
            self.rect.bottom = ALTO
        if self.rect.top < 0:
            self.rect.top = 0

            

    def disparar(self):
        bala = Bala(self.rect.centerx, self.rect.top)
        all_sprites.add(bala)
        balas.add(bala)

# ========================================
# CLASE bala
class Bala(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\laser1.png")
        self.image.set_colorkey(NEGRO)
        self.rect = self.image.get_rect()
        self.rect.y = y
        self.rect.centerx = x
        self.speedy = -10

    def update(self):
        self.rect.y += self.speedy
        if self.rect.bottom < 0:
            self.kill()
# ========================================
# FUNCIÓN PARA SALIR DEL JUEGO
def salir():    
    fuente = pygame.font.Font('freesansbold.ttf', 32)    
    texto_salir = fuente.render("Si desea salir... presione la tecla 's'", True, (195,157,154),(0,0,45))        
    while True:                   
        for evento in pygame.event.get():                         
            if evento.type == pygame.KEYDOWN:                 
                if evento.key == pygame.K_s:                     
                    quit()     
                else:
                    return 0                
        ventana.fill((158,185,65))        
        ventana.blit(texto_salir,(ANCHO//4,ALTO//2))
        pygame.display.flip()

# ========================================
# FOndo de pantalla
fondo = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\fondo03.png")

# ========================================
# Creación de la nave
all_sprites = pygame.sprite.Group()
balas = pygame.sprite.Group()
avion = Nave()
all_sprites.add(avion)


# ========================================
# variables
x=100
y=100
alto_rectangulo=50
ancho_rectangulo=50


# ========================================
# BUCLE PRINCIPAL DEL JUEGO
while True:
    # velocidad sujerida del juego
    clock.tick(120)
    # obtiene eventos de entrada
    for evento in pygame.event.get():                         
        if evento.type == pygame.KEYDOWN: 
            # Control de salida con la tecla escape
            if evento.key == pygame.K_ESCAPE: 
                salir()        

            if evento.key == pygame.K_SPACE: 
                print("disparar")                                           
                # Se carga un sonido
                pygame.mixer.Sound.play(bala)
                # Luego dispara
                avion.disparar() 


            
    # Actualiza los sprite
    all_sprites.update()
    #Renderiza fondo 
    ventana.blit(fondo,(0,0))

    # Dibuja un rectangulo(base,((RGB)), (x,y,ancho,alto))
    
    pygame.draw.rect(ventana,(255,255,255),(x,y,ancho_rectangulo,alto_rectangulo))

    y+=5    
    if y>ALTO:
        y=0
        x=random.randint(0,ANCHO)
        alto_rectangulo=random.randint(5,50)
        ancho_rectangulo=random.randint(10,70)


    all_sprites.draw(ventana)    
    # dibujando todo
    pygame.display.flip()