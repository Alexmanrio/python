# Importar la librería de Juegos
import pygame
# Inicia la libreía.
pygame.init()
# Crear una ventana de 1024X764
ventana=pygame.display.set_mode((1024,700))
# Reloj de sistema desde pygame
reloj=pygame.time.Clock()
# velocidad de fps
velocidad=1
# posición de coordenada
x=200
y=200

# ========================================
# Bucle principal

while True:
    # mover 1 fotograma en un segundo
    reloj.tick(velocidad)
    # Bucle para pedir eventos    		
    for evento in pygame.event.get():        
        # Compara si se presionó el boton X de la ventana
        if evento.type==pygame.QUIT:                
            # Salir
            quit()  

    #========================
    # LOGICA DEL PROGRAMA



    # =======================
    # PINTADO Y DIBUJADO
    # Pintar de color (R,G,B) El fondo
    ventana.fill((0,80,150))

    # Dibuja un rectangulo                 (x,y, ancho,alto)
    pygame.draw.rect(ventana,(255,255,255),(x,y,200,100))
    x=x+10
    
    # Actualiza la ventana, constantemente...
    pygame.display.update()