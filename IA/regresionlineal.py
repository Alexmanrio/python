import numpy as np 
import matplotlib.pyplot as plt
from pylab import *

#Se tiene los siguientes vectores, debe encontrar el punto de intersección
x = [1, 2, 3, 5, 6, 7, 8, 9]
y = [3, 4.3, 6, 6.5, 8, 8, 9.3, 8.7]

n = len(x)
x = np.array(x)
y = np.array(y)
sumx = sum(x)
sumy = sum(y)
sumx2 = sum(x*x)
sumy2 = sum(y*y)
sumxy = sum(x*y)
promx = sumx/n
promy = sumy/n

m = (sumx*sumy - n*sumxy)/(sumx**2 - n*sumx2)
b =  promy - m*promx

sigmax = np.sqrt(sumx2/n - promx**2)
sigmay = np.sqrt(sumy2/n - promy**2)
sigmaxy = sumxy/n - promx*promy
R2 = (sigmaxy/(sigmax*sigmay))**2

print("m=",round(m,2), " b=",round(b,2))
print("R2=",round(R2,2))

plt.plot(x, y,'bo', label='H1')
plt.plot(x, m*x + b, 'r-')  
plt.xlabel('x')
plt.show()