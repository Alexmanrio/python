# Importar la librería de Juegos
import pygame
# Inicia la libreía.
pygame.init()
# Crear una ventana de 1024X764
ventana=pygame.display.set_mode((1024,700))
# Reloj de sistema desde pygame
reloj=pygame.time.Clock()
# velocidad de fps
velocidad=70
# posición de coordenada
x=200
y=200
ancho=50
alto=50

incremento_x=7
incremento_y=7

color=(255,255,255)
contador=0
# ========================================
# Bucle principal
while True:
    # mover 1 fotograma en un segundo
    reloj.tick(velocidad)
    # Bucle para pedir eventos    		
    for evento in pygame.event.get():        
        # Compara si se presionó el boton X de la ventana
        if evento.type==pygame.QUIT:                
            # Salir
            quit()  

    #========================
    # LOGICA DEL PROGRAMA    
    
    x=x+incremento_x
    y=y+incremento_y
    if x>1024-ancho:
        incremento_x=incremento_x*-1
        contador+=1
        color=(255,0,0)
    if x<0:
        incremento_x=incremento_x*-1            
        contador+=1
        color=(0,255,0)
    if y<0:
        incremento_y=incremento_y*-1            
        contador+=1
        color=(0,0,255)
    if y>700-alto:
        incremento_y=incremento_y*-1            
        contador+=1
        color=(244,255,0)

    if contador>=30:
        incremento_x=0
        incremento_y=0
    # =======================
    # PINTADO Y DIBUJADO
    # Pintar de color (R,G,B) El fondo
    ventana.fill((0,80,150))
    # Dibuja un rectangulo                 (x,y, ancho,alto)
    pygame.draw.rect(ventana,color,(x,y,ancho,alto))   
    
    # Actualiza la ventana, constantemente...
    pygame.display.update()