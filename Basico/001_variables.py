print# Declaración de numero entero
a=20
# Declaración de numero real
b=3.6
# Declaración de cadenas
c="Hola"

# Asignación múltiple de valores a múltiples variables
a, b, c = 2, 3.6, "hola"
# Asignación del mismo valor a múltiples variables al mismo tiempo
x = y = z = True
print("hola",a)
print("Holanda")
h = "mundo"
print(h)

print("h=", h)