
from random import random
import pygame
import os
from Nave import *
import Bala
import Meteorito


ANCHO = 1024
ALTO = 700
DIMENSIONES=(ANCHO,ALTO)
NEGRO = (0, 0, 0)
pygame.init()
ventana = pygame.display.set_mode(DIMENSIONES)
pygame.display.set_caption("Aviones")
reloj = pygame.time.Clock()
velocidad=70
# Declaración de objetos
avion = Nave()


# ========================================
# BUCLE PRINCIPAL DEL JUEGO
while True:
    # velocidad sujerida del juego
    reloj.tick(velocidad)
    # obtiene eventos de entrada
    for evento in pygame.event.get():                         
        if evento.type == pygame.KEYDOWN: 
            # Control de salida con la tecla escape
            if evento.key == pygame.K_ESCAPE: 
                pygame.quit()
            
    pygame.display.flip()