#   Proyecto creado por TecnoProfe
#    youtube: https://www.youtube.com/tecnoprofe
import pygame
import os
# ========================================
# Crear pelota
class Pelota(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)        
        # Agrega una imagen
        self.image=pygame.image.load(os.path.dirname(__file__)+"\\img\\pelota1.png")
        # obtiene la dimensiones de la imagen
        self.rect=self.image.get_rect()
        self.rect.x=300
        self.rect.y=100

    def actualizar(self):
        keystate=pygame.key.get_pressed()
        if keystate[pygame.K_LEFT]:           
            self.rect.x-=5
        if keystate[pygame.K_RIGHT]:            
            self.rect.x+=5 
        if keystate[pygame.K_UP]:            
            self.rect.y-=5 
        if keystate[pygame.K_DOWN]:            
            self.rect.y+=5 

ANCHO = 800
ALTO = 600
DIMENSIONES=(ANCHO,ALTO)
# ====================================
# Colores
NEGRO = (0, 0, 0)
BLANCO = (255, 255, 255)

pygame.init()
pygame.mixer.init()
ventana = pygame.display.set_mode(DIMENSIONES)
pygame.display.set_caption("Laberinto")
clock = pygame.time.Clock()

# ====================================
# Mapa
mapa = ["XXXXXXXXXXXXXXXX",
        "X-------------XX",
        "XXXX--------XXXX",
        "XXXXXXXXXX-----X",
        "XXXX-----------X",
        "XX------XXXXXXXX",
        "XXXXXX------X-XX",
        "XX------XXX---XX",
        "XXXX-------X---X",
        "XXXXXXXXXXXXXXXX",
        "XXXXXX------XXXX",
        "XXXXXXXXXXXXXXXX",]

# ========================================
# Construir muros
ladrillo_img=pygame.image.load(os.path.dirname(__file__)+"\\img\\ladrillo.png")
ladrillo_dimensiones=ladrillo_img.get_rect()

def construir_mapa(mapa):
    muros=[]
    x,y=0,0
    for fila in mapa:
        for columna in fila:
            if columna=="X":                
                muros.append(pygame.Rect(x,y,50,50))                
            x+=50
        x=0
        y+=50
    return muros

def dibujar_mapa(ventana, muros):
    for muro in muros:
        pygame.draw.rect(ventana,BLANCO,muro)
        #Esta línea intercambia una imagen en un rectangulo.
        ventana.blit(ladrillo_img, muro)      

lista_muros=construir_mapa(mapa)

listaPelota=pygame.sprite.Group()
pelota=Pelota()
listaPelota.add(pelota)

# ========================================
# Juego Bucle
x=100
y=50

while True:
    # velocidad sujerida del juego
    clock.tick(10)
    # obtiene eventos de entrada
    for event in pygame.event.get():
        # Verifica si presionó el boton cerrar. 
        if event.type == pygame.QUIT:
            quit()


    #Renderiza fondo negro
    ventana.fill(NEGRO)
    pelota.actualizar()

    dibujar_mapa(ventana,lista_muros)

    for muro in lista_muros:
        choque = pelota.rect.colliderect(muro)
        if choque:               
            print("si")


    listaPelota.draw(ventana)
    
    # dibujando todo
    pygame.display.flip()