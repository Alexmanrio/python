# Ejercicio 20.	
# Escribe un programa para solicitar al usuario 
# que ingresé números enteros positivos 
# (la cantidad que ingresará no se conoce y la decide el usuario). 
# La lectura de números finalizará cuando el usuario ingrese el número -1. 
# Por cada número ingresado, mostrar la cantidad de dígitos pares 
# y la cantidad de dígitos impares que tiene. 
# 
# Al finalizar, mostrar cuántos números múltiplos de 3 ingresó el usuario.

# Entrada controlada de datos
lista=[]
bandera=True
while bandera==True:
    numero_entero=int(input("Introduzca numeros positivos enteros, y si presiona -1  termina el programa"))

    if numero_entero>=0:
        lista.append(numero_entero)
    elif numero_entero<-1:
        print("No se puede ingresar numeros negativos")

    if numero_entero==-1:
        bandera=False
        print("se termino")
# procesar datos

multiplo3=0
for numero in lista:
    if numero%2==0:
        print("El número ",numero, " es par")
    if numero%2==1:
        print("El número ",numero, " es impar")
    
    if numero%3==0:
        multiplo3=multiplo3+1
    
print("Los multiplos de 3 son: ",multiplo3)




