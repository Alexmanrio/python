#   Proyecto creado por TecnoProfe
#    youtube: https://www.youtube.com/tecnoprofe
import pygame

ANCHO = 800
ALTO = 600
DIMENSIONES=(ANCHO,ALTO)
# ====================================
# Colores
NEGRO = (0, 0, 0)
BLANCO = (152, 255, 255)

# ====================================
# Configuraciones iniciales.
pygame.init()
ventana = pygame.display.set_mode(DIMENSIONES)
pygame.display.set_caption("Laberinto")
reloj = pygame.time.Clock()

# Creación de mapa

mapa = ["-XXXXX-XXXXXXXXX",
        "X--------------X",
        "X--XXX-XXXXX-XXX",
        "XXXXXXXXXX-----X",
        "XXXX-----------X",
        "XX------XXXXXXXX",
        "XXXXXX------X-XX",
        "XX------XXX---XX",
        "XXXX-------X---X",
        "XXXXXXXXXXXXXXXX",
        "XXXXXX------XXXX",
        "XXXXXXXXXXXXXXXX",]

mapa1 = ["XXXXXXXXXXXXXXXX",
        "X--------------X",
        "X--XXX-XXXXX-XXX",
        "XXXXXXXXXX-----X",
        "XXXX-----------X",
        "XX------XXXXXXXX",
        "XXXXXX------X-XX",
        "XX------XXX---XX",
        "XXXX-------X---X",
        "XXXXXXXXXXXXXXXX",
        "XXXXXX------XXXX",
        "XXXXXXXXXXXXXXXX",]


# ========================================
# Construir muros
def construir_mapa(mapa):
    muros=[]
    x=0
    y=0
    for fila in mapa:
        for ladrillo in fila:
            if ladrillo=="X":
                muros.append(pygame.Rect(x,y,50,50))
            x+=50
        x=0
        y+=50
    return muros
    
lista_muros=construir_mapa(mapa)

def dibujar_mapa(ventana, muros):
    for muro in muros:
        pygame.draw.rect(ventana,BLANCO,muro)

# ========================================
# Juego Bucle
while True:
    # velocidad sujerida del juego
    reloj.tick(120)
    # obtiene eventos de entrada
    for event in pygame.event.get():
        # Verifica si presionó el boton cerrar. 
        if event.type == pygame.QUIT:
            quit()

    #Renderiza fondo noegro
    ventana.fill(NEGRO)
    dibujar_mapa(ventana,lista_muros)        
    # dibujando todo
    pygame.display.flip()