#   Proyecto creado por TecnoProfe
#    youtube: https://www.youtube.com/tecnoprofe
import pygame
import random

ANCHO = 800
ALTO = 600
DIMENSIONES=(ANCHO,ALTO)
# ====================================
# Colores
NEGRO = (0, 0, 0)
BLANCO = (152, 255, 255)

# ====================================
# Configuraciones iniciales.
pygame.init()
ventana = pygame.display.set_mode(DIMENSIONES)
pygame.display.set_caption("Serpiente v1")
reloj = pygame.time.Clock()

x=random.randrange(1, ANCHO-30)
y=random.randrange(1, ALTO-30)
print("X=",x," Y=",y)

serpiente_X=400
serpiente_Y=300

direccion="sin direccion"
# ========================================
# Juego Bucle
while True:
    # velocidad sujerida del juego
    reloj.tick(24)
    # obtiene eventos de entrada
    for event in pygame.event.get():
        # Verifica si presionó el boton cerrar. 
        if event.type == pygame.QUIT:
            quit()
        
        tecla = pygame.key.get_pressed()   
        if tecla[pygame.K_LEFT]:
            direccion="izquierda"            
        if tecla[pygame.K_UP]:
            direccion="arriba"            
        if tecla[pygame.K_RIGHT]:
            direccion="derecha"            
        if tecla[pygame.K_DOWN]:
            direccion="abajo"            
        

    #LOGICA

    #Renderiza fondo negro
    ventana.fill(NEGRO)    
    #Dibujar comida
    pygame.draw.rect(ventana, BLANCO, pygame.Rect(x, y, 30, 30))
    
    # Dibujar serpiente
    if direccion=="izquierda":
        serpiente_X-=30
        pygame.draw.rect(ventana, (0,180,70), pygame.Rect(serpiente_X, serpiente_Y, 30, 30))
    if direccion=="derecha":
        serpiente_X+=30
        pygame.draw.rect(ventana, (0,180,70), pygame.Rect(serpiente_X, serpiente_Y, 30, 30))
    if direccion=="arriba":
        serpiente_Y-=30
        pygame.draw.rect(ventana, (0,180,70), pygame.Rect(serpiente_X, serpiente_Y, 30, 30))
    if direccion=="abajo":
        serpiente_Y+=30
        pygame.draw.rect(ventana, (0,180,70), pygame.Rect(serpiente_X, serpiente_Y, 30, 30))
    
    
    pygame.display.flip()