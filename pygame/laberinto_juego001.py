#   Proyecto creado por TecnoProfe
#    youtube: https://www.youtube.com/tecnoprofe
#================================================
from sqlite3 import Timestamp
import pygame
import os
import random

pygame.init()
ANCHO = 800
ALTO = 600
DIMENSIONES=(ANCHO,ALTO)
ventana = pygame.display.set_mode(DIMENSIONES)
# ====================================
# Colores
NEGRO = (0, 0, 0)
BLANCO = (152, 255, 255)
#====================================
# Configuraciones iniciales.
pygame.display.set_caption("Juego Programación II")
reloj = pygame.time.Clock()
velocidad=20

x_antiguo=0
y_antiguo=0

fondo=pygame.image.load(os.path.dirname(__file__)+"\\img\\fondoestrellado01.png")
#====================================
# Jugadores
jugador=pygame.Rect(0,25,25,25)
x_comida=random.randrange(0,ANCHO-25)
y_comida=random.randrange(0,ALTO-25)

comida=pygame.Rect(x_comida,y_comida,30,30)
canasta=0
cronometro=100
#creación de una fuente de letra
fuente = pygame.font.Font('freesansbold.ttf', 18)    


#====================================
# Creación de mapas
mapa = ["-OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
        "-------------------------------O",
        "O------------------------------O",
        "O------OOOOOO---OOOOOO---------O",
        "O------------------------------O",
        "O------------------------------O",
        "O-----------OOOOOOOOOOOOOOOO---O",
        "O------------------------------O",
        "OOOOOOOOOOOOOOOOOOOOOOOOO------O",
        "O------------------------------O",
        "OOOOOOOOOOOOO--OOOOOOOOOOOOOO--O",
        "O------------------------------O",
        "O------------------------------O",
        "O------------------------------O",
        "O------------------------------O",
        "O------------------------------O",
        "O------------------------------O",
        "O------------------------------O",
        "O------------------------------O",
        "O------------------------------O",
        "O------------------------------O",
        "O------------------------------O",
        "O------------------------------O",
        "OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",]
# ========================================
# FUNCIONES
def construir_mapa():
    muros=[]
    x,y=0,0
    for fila_ladrillos in mapa:
        for ladrillo in fila_ladrillos:
            if ladrillo=="O":
                muros.append(pygame.Rect(x,y,25,25))
            x+=25
        x=0
        y+=25
    return muros


# ========================================
# Juego Bucle
while True:
    # velocidad sujerida del juego
    reloj.tick(velocidad)
    # obtiene eventos de entrada
    for event in pygame.event.get():
        # Verifica si presionó el boton cerrar. 
        if event.type == pygame.QUIT:
            quit()

    tecla=pygame.key.get_pressed()                
    if tecla[pygame.K_LEFT]:            
        jugador.left -= 25
    if tecla[pygame.K_RIGHT]:            
        jugador.left += 25        
    if tecla[pygame.K_UP]:            
        jugador.top -= 25       
    if tecla[pygame.K_DOWN]:            
        jugador.top += 25        


    
    
# ========================================
# DIBUJAR PANTALLA
    #Renderiza fondo negro
    ventana.fill(NEGRO)    
    ventana.blit(fondo,(0,0))
    
    #Dibujar ladrillos
    for ladrillo in construir_mapa():
        pygame.draw.rect(ventana,BLANCO,ladrillo)

    #control de colisiones
    for ladrillo in construir_mapa():                
        if jugador.colliderect(ladrillo):            
            jugador.left=x_antiguo
            jugador.top=y_antiguo
    #control de colisiones para comida
    if jugador.colliderect(comida):
        comida.left=random.randrange(0,ANCHO-25)
        comida.top=random.randrange(0,ALTO-25)
        canasta+=1
        velocidad+=3
        cronometro=100

    #Dibujar comida    
    pygame.draw.rect(ventana,(230,50,80),comida)

    x_antiguo=jugador.left  # Guarda el punto x del jugador
    y_antiguo=jugador.top   # Guarda el punto y del jugador

    #Dibujar texto

    texto_vida = fuente.render('Vida= '+str(canasta), True, (0,255,0), (0,0,0))
    ventana.blit(texto_vida,(50,0))   

    texto_vida = fuente.render('Velocidad= '+str(velocidad), True, (0,255,0), (0,0,0))
    ventana.blit(texto_vida,(120,0))   

    #cronometro
    segundos=(pygame.time.get_ticks()-1000)/1000 
    
    if round(segundos)< cronometro:
        cronometro-=1       
    if cronometro<0:
        print("perdio")
        
    texto_vida = fuente.render('Cronometro= '+str(cronometro), True, (0,255,0), (0,0,0))
    ventana.blit(texto_vida,(250,0))   

    pygame.draw.rect(ventana, (0,255,0),jugador)    
    pygame.display.flip()