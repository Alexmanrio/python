import time
import tkinter as tk




class Game:
    def __init__(self):
        self.initialize_game()

    def initialize_game(self):
        self.current_state = [['.','.','.'],
                              ['.','.','.'],
                              ['.','.','.']]

        # Player X always plays first
        self.player_turn = 'X'        


    def draw_board(self):
        for i in range(0, 3):
            for j in range(0, 3):
                print('{}|'.format(self.current_state[i][j]), end=" ")                
            print()
        print()

        caja1.config(text=self.current_state[0][0])
        caja2.config(text=self.current_state[0][1])
        caja3.config(text=self.current_state[0][2])
        caja4.config(text=self.current_state[1][0])
        caja5.config(text=self.current_state[1][1])
        caja6.config(text=self.current_state[1][2])
        caja7.config(text=self.current_state[2][0])
        caja8.config(text=self.current_state[2][1])
        caja9.config(text=self.current_state[2][2])
        



    # Determines if the made move is a legal move
    def is_valid(self, px, py):
        if px < 0 or px > 2 or py < 0 or py > 2:
            return False
        elif self.current_state[px][py] != '.':
            return False
        else:
            return True
            

    # Checks if the game has ended and returns the winner in each case
    def is_end(self):
        # Vertical win
        for i in range( 0,3):
            if (self.current_state[0][i] != '.' and
                self.current_state[0][i] == self.current_state[1][i] and
                self.current_state[1][i] == self.current_state[2][i]):
                return self.current_state[0][i]

        # Horizontal win
        for i in range(0, 3):
            if (self.current_state[i] == ['X', 'X', 'X']):
                return 'X'
            elif (self.current_state[i] == ['O', 'O', 'O']):
                return 'O'

        # Main diagonal win
        if (self.current_state[0][0] != '.' and
            self.current_state[0][0] == self.current_state[1][1] and
            self.current_state[0][0] == self.current_state[2][2]):
            return self.current_state[0][0]

        # Second diagonal win
        if (self.current_state[0][2] != '.' and
            self.current_state[0][2] == self.current_state[1][1] and
            self.current_state[0][2] == self.current_state[2][0]):
            return self.current_state[0][2]

        # Is whole board full?
        for i in range(0, 3):
            for j in range(0, 3):
                # There's an empty field, we continue the game
                if (self.current_state[i][j] == '.'):
                    return None

        # It's a tie!
        return '.'

    # Player 'O' is max, in this case AI
    def max(self):

        # Possible values for maxv are:
        # -1 - loss
        # 0  - a tie
        # 1  - win

        # We're initially setting it to -2 as worse than the worst case:
        maxv = -2

        px = None
        py = None

        result = self.is_end()

        # If the game came to an end, the function needs to return
        # the evaluation function of the end. That can be:
        # -1 - loss
        # 0  - a tie
        # 1  - win
        if result == 'X':
            return (-1, 0, 0)
        elif result == 'O':
            return (1, 0, 0)
        elif result == '.':
            return (0, 0, 0)

        for i in range(0, 3):
            for j in range(0, 3):
                if self.current_state[i][j] == '.':
                    # On the empty field player 'O' makes a move and calls Min
                    # That's one branch of the game tree.
                    self.current_state[i][j] = 'O'
                    (m, min_i, min_j) = self.min()
                    # Fixing the maxv value if needed
                    if m > maxv:
                        maxv = m
                        px = i
                        py = j
                    # Setting back the field to empty
                    self.current_state[i][j] = '.'
        return (maxv, px, py)

    # Player 'X' is min, in this case human
    def min(self):

        # Possible values for minv are:
        # -1 - win
        # 0  - a tie
        # 1  - loss

        # We're initially setting it to 2 as worse than the worst case:
        minv = 2

        qx = None
        qy = None

        result = self.is_end()

        if result == 'X':
            return (-1, 0, 0)
        elif result == 'O':
            return (1, 0, 0)
        elif result == '.':
            return (0, 0, 0)

        for i in range(0, 3):
            for j in range(0, 3):
                if self.current_state[i][j] == '.':
                    self.current_state[i][j] = 'X'
                    (m, max_i, max_j) = self.max()
                    if m < minv:
                        minv = m
                        qx = i
                        qy = j
                    self.current_state[i][j] = '.'

        return (minv, qx, qy)

    def play(self):
    
        
        self.result = self.is_end()

        # Printing the appropriate message if the game has ended
        if self.result != None:
            if self.result == 'X':
                print('Ganaste Humano X!')
            elif self.result == 'O':
                print('Gano la Inteligencia Artificial O!')
            elif self.result == '.':
                print("Empate!")

            self.initialize_game()
            return

        # If it's player's turn
        if self.player_turn == 'X':
            while True:
                start = time.time()
                (m, qx, qy) = self.min()
                end = time.time()
                print('Tiempo de evaluación: {}s'.format(round(end - start, 7)))
                print('Movimiento reecomendado: X = {}, Y = {}'.format(qx, qy))

                                    
                px = int(input('Inserte la coordenada X: '))
                py = int(input('Inserte la coordenada Y: '))

                (qx, qy) = (px, py)

                if self.is_valid(px, py):
                    self.current_state[px][py] = 'X'
                    self.player_turn = 'O'
                    break
                else:
                    print('Este movimiento no es valido! Intente nuvamente.')

        # If it's AI's turn
        else:
            (m, px, py) = self.max()
            self.current_state[px][py] = 'O'
            self.player_turn = 'X'

        self.draw_board()

    def turno(self,x,y):
        self.result = self.is_end()

        # Printing the appropriate message if the game has ended
        if self.result != None:
            if self.result == 'X':
                print('Ganaste Humano X!')
            elif self.result == 'O':
                print('Gano la Inteligencia Artificial O!')
            elif self.result == '.':
                print("Empate!")

            self.initialize_game()
            return

        # If it's player's turn
        if self.player_turn == 'X':
        
            start = time.time()
            (m, qx, qy) = self.min()
            end = time.time()            
                                
            px = int(x)
            py = int(y)

            (qx, qy) = (px, py)

            if self.is_valid(px, py):
                self.current_state[px][py] = 'X'
                self.player_turn = 'O'                
            else:
                print('Este movimiento no es valido! Intente nuvamente.')

        # If it's AI's turn
        else:
            (m, px, py) = self.max()
            self.current_state[px][py] = 'O'
            self.player_turn = 'X'

        self.draw_board()

g = Game()


    

def main():
    # print("estoy en el método main")    
    g.play()
if __name__ == "__main__":
    from tkinter import *    

    ventana=tk.Tk()
    ventana.geometry("800x600")

    etiqueta=Label(ventana,text="saludo etiqueta")
    etiqueta.pack()

    boton1=Button(ventana,text="Humano",command=main)
    boton1.place(x=100,y=100)

    boton2=Button(ventana,text="IA",command=main)
    boton2.place(x=200,y=100)

    #boton 1
    caja1=Button(ventana,text=" ")
    caja1.place(x=100,y=200)
    #boton 2
    caja2=Button(ventana,text=" ")
    caja2.place(x=130,y=200)
    #boton 3
    caja3=Button(ventana,text=" ")
    caja3.place(x=160,y=200)

    #boton 4
    caja4=Button(ventana,text=" ")
    caja4.place(x=100,y=230)
    #boton 5
    caja5=Button(ventana,text=" ")
    caja5.place(x=130,y=230)
    #boton 6
    caja6=Button(ventana,text=" ")
    caja6.place(x=160,y=230)

    #boton 7
    caja7=Button(ventana,text=" ")
    caja7.place(x=100,y=260)
    #boton 8
    caja8=Button(ventana,text=" ")
    caja8.place(x=130,y=260)
    #boton 9
    caja9=Button(ventana,text=" ")
    caja9.place(x=160,y=260)

    ventana.mainloop()

    # print("iniciando el código")
    # main()    