import pygame
from pygame.locals import *
 
pygame.init()
 
pantalla = pygame.display.set_mode((800,600),0,20)
imagen = pygame.image.load("bolivia.png")
 
x = 10
y = 10
 
sprite1 = pygame.sprite.Sprite()
sprite1.image = imagen
 
reloj = pygame.time.Clock()
while True:
    for eventos in pygame.event.get():
        if eventos.type == pygame.QUIT:
            exit()
 
    reloj.tick(25)
    
    pantalla.fill((255,0,0)) #Color de fondo
        #Color     R,G,B
        #Ejemplo 255,0,0 es el color rojo    
        
    pantalla.blit(sprite1.image,(x,y))

    x,y = pygame.mouse.get_pos()
    #Recupera las coordenadas x,y del mouse    
    
    pygame.display.update()
    #Actualiza los graficos