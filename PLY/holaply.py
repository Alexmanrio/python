#importamos la libreria ply.lex como lex
import ply.lex as lex

# lista de tokens
tokens = [
         'NUMERO',
         'ADMIRACION',  #!
         'MAS',  #!
         'RECONOCEC',  
         'COMENTARIO',
         'MENOS'

         ]          

t_RECONOCEC=r"C"
t_MAS=r"\+"
t_ADMIRACION=r"\!"
t_NUMERO=r"[0-9]+$"
t_COMENTARIO=r"\/\/.*"
t_MENOS=r"\-"

# Crea un objeto lexer para el analisis lexico
lexico = lex.lex()

# Introducimos la cadena a ser evaluada
texto=input("introduzca una cadena: ")
lexico.input(texto)



#reglas de produccion

def p_expresion_binaria(t):
    '''expresion_numerica : expresion_numerica MAS expresion_numerica
                        | expresion_numerica MENOS expresion_numerica
                        | expresion_numerica POR expresion_numerica
                        | expresion_numerica DIVIDIDO expresion_numerica'''

def p_expresion_unaria(t):
    'expresion_numerica : MENOS expresion_numerica'


# verificar en un bucle las cadenas validas con respecto a las reglas
while True:
    tok = lexico.token()
    if not tok: 
        break      # si no hay mas tokens termina el analisis
    print(tok)