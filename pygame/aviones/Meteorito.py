import pygame
import os
# ========================================
# CLASE Meteorito
class Meteorito(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load(fotos_meteoritos[random.randint(0,4)])
        self.image.set_colorkey((0,0,0))
        #Obtiene las coordenadas de la imagen en un rectangulo.
        self.rect = self.image.get_rect()
        # en el eje x del rectangulo coloca
        self.rect.x = random.randrange(ANCHO - self.rect.width)
        self.rect.y = random.randrange(-150,-50)
        self.velocidad_y = random.randrange(1, 7)
        self.velocidad_x = random.randrange(-7, 7)

    def update(self):
        self.rect.x += self.velocidad_x
        self.rect.y += self.velocidad_y
        if self.rect.top > ALTO + 10 or self.rect.left < -25 or self.rect.right > ANCHO + 22 :
            self.rect.x = random.randrange(ANCHO - self.rect.width)
            self.rect.y = random.randrange(-150, -50)
            self.velocidad_y = random.randrange(1, 7)
            self.velocidad_x = random.randrange(-7, 7)