import pygame   # Importar la librería Pygame
import os       # Importar la librería para manejo del Sistema Operativo
import random   # Impartar la librería para generar números aleatorios.

ANCHO= 800
ALTO= 600

COLOR = pygame.Color('red') #Color de fondo
FPS = 80 #Frames por segundo

pygame.init()   # iniciar pygame
ventana = pygame.display.set_mode((ANCHO,ALTO))   # Tamaño de la ventana 

# clase sprite
#=============================
class Sprite(pygame.sprite.Sprite):
    def __init__(self,x,y):
        super().__init__()

        self.imagenes = []   # crea una lista.                
        self.imagenes.append(pygame.image.load(os.path.dirname(__file__)+'\\img\\walk\\walk1.png'))
        self.imagenes.append(pygame.image.load(os.path.dirname(__file__)+'\\img\\walk\\walk2.png'))
        self.imagenes.append(pygame.image.load(os.path.dirname(__file__)+'\\img\\walk\\walk3.png'))
        self.imagenes.append(pygame.image.load(os.path.dirname(__file__)+'\\img\\walk\\walk4.png'))
        self.imagenes.append(pygame.image.load(os.path.dirname(__file__)+'\\img\\walk\\walk5.png'))
        self.imagenes.append(pygame.image.load(os.path.dirname(__file__)+'\\img\\walk\\walk6.png'))
        self.imagenes.append(pygame.image.load(os.path.dirname(__file__)+'\\img\\walk\\walk7.png'))
        self.imagenes.append(pygame.image.load(os.path.dirname(__file__)+'\\img\\walk\\walk8.png'))
        self.imagenes.append(pygame.image.load(os.path.dirname(__file__)+'\\img\\walk\\walk9.png'))
        self.imagenes.append(pygame.image.load(os.path.dirname(__file__)+'\\img\\walk\\walk10.png'))

        self.index = 0
        
        self.x=x
        self.y=y
        self.image = self.imagenes[self.index] 
        self.rect = pygame.Rect(x,y, 150, 150)
 
    def update(self):        
        #Control de teclas cursoras    
        tecla = pygame.key.get_pressed()   
        if tecla[pygame.K_LEFT]:            
            if self.index < 0:                
                self.index = 10                    
            self.index -= 1
            self.image = self.imagenes[self.index]            
            self.rect.x-=3
            print("index: " ,self.index,"  len imagenes:",len(self.imagenes))

        if tecla[pygame.K_RIGHT]:                                               
            if self.index > len(self.imagenes)-1:
                self.index = 0                    
            self.image = self.imagenes[self.index]
            self.index +=1
            self.rect.x+=3
            print("index: " ,self.index,"  len imagenes:",len(self.imagenes))

class Bloque(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()     
        self.image=pygame.image.load(pygame.image.load(os.path.dirname(__file__)+'\\img\\walk\\walk1.png'))
        self.rect = self.image.get_rect()
        self.x=random.randint(0,ANCHO)
        self.y=0
        self.ancho=random.randint(50,70)
        self.alto=random.randint(50,70)        

    def update(self):        
        # Dibuja un rectangulo(base,((RGB)), (x,y,ancho,alto))    
        pygame.draw.rect(ventana,(255,45,255),(self.x,self.y,self.ancho,self.alto))
        self.y+=5    
        if self.y>ALTO:
            self.y=0
            self.x=random.randint(0,ANCHO)
            self.alto=random.randint(5,50)
            self.ancho=random.randint(10,70)


# ========================================
# Declaración de grupos de sprite's
todos_los_sprites = pygame.sprite.Group()
lista_bloques = pygame.sprite.Group()
# Declaración de objetos
geronimo = Sprite(ANCHO//2,ALTO-200)

todos_los_sprites.add(geronimo)


for cuadrado in range(7):
    bloque=Bloque()
    lista_bloques.add(bloque)
    todos_los_sprites.add(bloque)


clock = pygame.time.Clock()

while True:
    for evento in pygame.event.get():
        if evento.type == pygame.QUIT:
            pygame.quit()
            quit()

    # Tiempo 
    clock.tick(FPS)
    
    #Renderiza fondo 
    ventana.fill(COLOR)
    
    # Colisiones meteorito con el avion
    choques = pygame.sprite.spritecollide(geronimo,lista_bloques, True) 
    if choques:
        print("Choque")
    
    # dibujando todo
    todos_los_sprites.draw(ventana)    
    
    # Actualiza los sprite
    todos_los_sprites.update()
    # Actualizar las animaciones.
    pygame.display.flip()                